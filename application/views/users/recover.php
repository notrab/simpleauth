<h2>Change password</h2>
<?= form_open('users/recover/'.$token); ?>
<?= validation_errors(); ?>
<? if(isset($error)): ?>
	<p><?= $error; ?></p>
<? endif; ?>

<p>
	<?= form_label('Token', 'token'); ?><br />
	<?= form_input('token', set_value('token', $token)); ?>
</p>

<p>
	<?= form_label('Password', 'password'); ?><br />
	<?= form_password('password', set_value('password')); ?>
</p>
<p>
	<?= form_label('Password Confirmation', 'password_confirmation'); ?><br />
	<?= form_password('password_confirmation', set_value('password_confirmation')); ?>
</p>
<p>
	<?= form_submit('submit', 'Change password'); ?>
</p>

<?= form_close(); ?>