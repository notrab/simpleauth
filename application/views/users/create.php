<h2>Sign up</h2>
<?= form_open('users/create'); ?>
<?= validation_errors(); ?>
<p>
	<?= form_label('Name', 'name'); ?><br />
	<?= form_input('name', set_value('name')); ?>
</p>
<p>
	<?= form_label('Username', 'username'); ?><br />
	<?= form_input('username', set_value('username')); ?>
</p>
<p>
	<?= form_label('Email', 'email'); ?><br />
	<?= form_input('email', set_value('email')); ?>
</p>
<p>
	<?= form_label('Password', 'password'); ?><br />
	<?= form_password('password', set_value('password')); ?>
</p>
<p>
	<?= form_label('Password Confirmation', 'password_confirmation'); ?><br />
	<?= form_password('password_confirmation', set_value('password_confirmation')); ?>
</p>
<p>
	<?= form_submit('submit', 'Sign up'); ?>
</p>

<?= form_close(); ?>