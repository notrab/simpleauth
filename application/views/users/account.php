<h2>Change account</h2>
<?= form_open('users/account'); ?>
<?= validation_errors(); ?>
<? if(isset($error)): ?>
	<p><?= $error; ?></p>
<? endif; ?>
<p>
	<?= form_label('Name', 'name'); ?><br />
	<?= form_input('name', set_value('name',  current_user()->name)); ?>
</p>
<p>
	<?= form_label('Location', 'location'); ?><br />
	<?= form_input('location', set_value('location', current_user()->location)); ?>
</p>
<p>
	<?= form_label('Username', 'username'); ?><br />
	<?= form_input('username', set_value('username',  current_user()->username)); ?>
</p>
<p>
	<?= form_label('Email', 'email'); ?><br />
	<?= form_input('email', set_value('email',  current_user()->email)); ?>
</p>

<p>
	<?= form_label('New Password', 'password'); ?><br />
	<?= form_password(array('name' => 'password', 'value' => '', 'autocomplete' => 'off')); ?>
</p>


<p style="border: 1px solid red; padding: 10px;">
	You must provide your current password to save changes.<br />
	<?= form_password('current_password', set_value('current_password')); ?>
</p>

<p>
	<?= form_submit('submit', 'Save'); ?>
</p>

<?= form_close(); ?>