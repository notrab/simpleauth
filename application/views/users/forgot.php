<h2>Reset forgotten password</h2>
<?= form_open('users/forgot'); ?>
<?= validation_errors(); ?>
<? if(isset($error)): ?>
	<p><?= $error; ?></p>
<? endif; ?>
<p>
	<?= form_label('Email', 'email'); ?><br />
	<?= form_input('email', set_value('email')); ?>
</p>

<p>
	<?= form_submit('submit', 'Recover'); ?>
</p>

<?= form_close(); ?>