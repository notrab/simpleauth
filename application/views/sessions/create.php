<h2>Login</h2>
<?= form_open('sessions/create'); ?>
<?= validation_errors(); ?>
<? if(isset($error)): ?>
	<p><?= $error; ?></p>
<? endif; ?>
<p>
	<?= form_label('Username/Email', 'identifier'); ?><br />
	<?= form_input('identifier', set_value('identifier')); ?>
</p>
<p>
	<?= form_label('Password', 'password'); ?><br />
	<?= form_password('password', set_value('password')); ?>
</p>
<p>
	<?= form_checkbox('remember', 'true'); ?> Remember me
</p>
<p>
	<?= form_submit('submit', 'Login'); ?> or <?= anchor(forgot_password_url(), "Forgotten password?"); ?>
</p>

<?= form_close(); ?>