<?php

class User_Model extends CI_Model {
	
	public $id = null;
	public $name = '';
	public $username = '';
	public $email = '';
	public $password = '';
	// users.remember_token
	
	public function all() {
		return $this->db->get('users');
	}
	
	public function find($conditions = array(), $limit = false, $offset = false) {
		
		if(is_numeric($conditions)) {
			// Get user based on id
			$this->db->select('*');
			$this->db->where('id', $conditions);
			$this->db->limit(1);
			$this->db->from('users');
			$query = $this->db->get();
			
			if($query->num_rows() > 0) {
				return $query->row();
			}
			else {
				return false;
			}
		}
		else {
			// Select based on conditions
			$this->db->select('*');
			$this->db->where($conditions);
			
			if($limit && is_numeric($limit))
				$this->db->limit($limit);
				
			if($offset && is_numeric($offset))
				$this->db->offset($offset);
				
			$this->db->from('users');
			return $this->db->get();
		}
	}
	
	
	public function insert($data) {
		$insert = $this->db->insert('users', $data);
		
		if($insert) {
			return $this->db->insert_id();
		}
		else {
			return false;
		}
	}
	
	public function update($user_id, $data) {
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);
	}
	
	public function find_by_identifier($identifier) {
		$this->db->select('*');
		$this->db->where('username', $identifier);
		$this->db->or_where('email', $identifier);
		$this->db->limit(1);
		$this->db->from('users');
		$query = $this->db->get();
		
		if($query->num_rows() > 0) {
			return $query->row();
		}
		else {
			return false;
		}
	}
	
	public function set_remember_token($user_id, $remember_token) {
		$this->db->set('remember_token', $remember_token);
		$this->db->where('id', $user_id);
		$this->db->update('users');
	}
	
	public function set_reset_token($user_id, $reset_token) {
		$this->db->set('reset_token', $reset_token);
		$this->db->where('id', $user_id);
		$this->db->update('users');
	}
	
}