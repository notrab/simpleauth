<?php

class Users extends MY_Controller {
	
	public function create() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirmation]');
		$this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required');
		
		if ($this->form_validation->run()) {
			
			$user = array(
				'name' => $this->input->post('name'),
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email'),
				'password' => $this->input->post('password'),
			);
			
			if($this->auth->new_user($user)) {
				// Sign the user in
				$this->auth->sign_in($user['username'], $user['password']);
				
				redirect('welcome/index');
			}
		}

		$this->load->view('users/create', $this->data);
	}
	
	public function forgot() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		
		if ($this->form_validation->run()) {
			$email =  $this->input->post('email');
			
			$user = $this->user_model->find(array('email' => $email));
			
			if($user->num_rows() > 0) {
				$this->auth->send_reset($user->row());
				
				redirect('users/forgot_sent');
			}
			else {
				$this->data['error'] = "Email not found";
			}
		}
		
		$this->load->view('users/forgot', $this->data);
	}
	
	public function forgot_sent() {
		$this->load->view('users/forgot_sent', $this->data);		
	}
	
	public function recover($token) {
		
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('token', 'Token', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[password_confirmation]');
		$this->form_validation->set_rules('password_confirmation', 'Password Confirmation', 'required');
		
		$this->data['token'] = $token;
		
		if($this->form_validation->run()) {
			if($user = $this->auth->recover($token, $this->input->post('password'))) {
				
				// Sign user in
				$this->auth->sign_in($user->username, $this->input->post('password'));
				
				redirect('welcome/index');	
			}
			else {
				$this->data['error'] = "Password could not be reset. Token may be invalid.";
			}
		}
		
		$this->load->view('users/recover', $this->data);
	}
	
	public function account() {
		$this->authorize();
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required');
		
		// Validate username
		$this->form_validation->set_rules('username', 'Username', 'required');
		if($this->current_user()->username != $this->input->post('username'))
			$this->form_validation->set_rules('username', 'Username', 'is_unique[users.username]');
			
		// Validate email
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		if($this->current_user()->email != $this->input->post('email'))
			$this->form_validation->set_rules('email', 'Email', 'is_unique[users.email]');
			
		$this->form_validation->set_rules('current_password', 'Current Password', 'required');
		
		if ($this->form_validation->run()) {
			
			$current_password = $this->auth->hash_password(
				$this->input->post('current_password'), 
				$this->current_user()->salt
			);
			
			if($current_password == $this->current_user()->password) {
				
				$data = array(
					'name' => $this->input->post('name'),
					'location' => $this->input->post('location'),
					'username' => $this->input->post('username'),
					'email' => $this->input->post('email')
				);
				
				$password = $this->input->post('password');
				if($password && trim($password) != '') {
					$data['password'] = $this->auth->hash_password($password, $this->current_user()->salt);
				}
				
				// Update rows
				$this->user_model->update($this->current_user()->id, $data);
				
				redirect('welcome/index');
			}
			else {
				$this->data['error'] = 'Your current password was incorrect.';
			}
		}
		
		$this->load->view('users/account', $this->data);
	}
	
}