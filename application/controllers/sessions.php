<?php

class Sessions extends MY_Controller {
	
	public function create() {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('identifier', 'Username/Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run()) {
			$identifier = $this->input->post('identifier');
			$password = $this->input->post('password');
			$remember = (bool) $this->input->post('remember');
			
			if($this->auth->sign_in($identifier, $password, $remember)) {
				redirect('welcome/index');
			}
			else {
				$this->data['error'] = 'The username/email and password combination is invalid.';
			}
		}

		$this->load->view('sessions/create', $this->data);
	}

	
	public function destroy() {
		$this->auth->sign_out();
		redirect('welcome/index');
	}
	
}