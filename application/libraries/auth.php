<?php

class Auth {
	
	private $CI;
	private $current_user = null;
	
	public function __construct() {
		$this->CI = &get_instance();
		$this->CI->load->library('session');
		$this->CI->load->helper(array('form', 'auth'));
		$this->CI->load->model('user_model');
	}
	
	public function new_user($data) {		
		$salt = $this->generate_random();
		
		// Store the salt
		$data['salt'] = $salt;
		
		// Hash user password
		$password = $data['password'];
		$data['password'] = $this->hash_password($password, $salt);		
		
		return $this->CI->user_model->insert($data);
	}
	
	public function sign_in($identifier, $password, $remember = false) {
		$user = $this->CI->user_model->find_by_identifier($identifier);
		
		if($user) {
			$hashed = $this->hash_password($password, $user->salt);
			
			// Match the passwords
			if($user->password == $hashed) {
				
				// Store the session
				$this->CI->session->set_userdata('user_id', $user->id);
				
				if($remember) {
					// Store a cookie field
					$remember_token = $this->generate_random();
										
					$this->CI->user_model->set_remember_token($user->id, $remember_token);
					setcookie('remember_token', $user->id.":".$remember_token, time() + 60 * 60 * 24 * 30, '/');  
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	public function sign_out() {
		$this->current_user = null;
		// Clear cookie
		setcookie("remember_token", "", time() - 3600, '/');
		
		// Clear session
		$this->CI->session->unset_userdata('user_id');
	}
	
	public function current_user() {
		
		if($this->current_user !== null) {
			return $this->current_user;
		}
		else {		
			$user = false;
			
			// Session
			$user_id = $this->CI->session->userdata('user_id');
			
			// Cookie
			$remember_token = isset($_COOKIE['remember_token']) ? $_COOKIE['remember_token'] : false;
						
			// If the session has been set
			if($user_id) {
				// Fetch the user from the database
				$user = $this->CI->user_model->find($user_id);
				
				
			}
			elseif($remember_token && !empty($remember_token)) {
				list($user_id, $token) = explode(":", $remember_token);
				
				// Get user from database
				$user = $this->CI->user_model->find($user_id);
				if($user && $user->remember_token != $token) {
					$user = false;
				}
			}
			
			if($user) {
				$this->current_user = $user;
				return $user;
			}
		
		}
		
		return null;
	}
	
	public function signed_in() {
		return is_object($this->current_user());
	}
	
	public function send_reset($user) {
		// Generate reset code
		$reset_token = $this->generate_random();
		
		// Store token with user
		$this->CI->user_model->set_reset_token($user->id, $reset_token);
		
		// Send email
		$this->CI->load->library('email', array('mailtype' => 'html'));

		$this->CI->email->from('reset@site.com', 'SiteName');
		$this->CI->email->to($user->email);
		$this->CI->email->subject('Reset password');
		$this->CI->email->message($this->CI->load->view('users/forgot_email', array('user' => $user, 'token' => $reset_token), true));

		$this->CI->email->send();
	}
	
	public function recover($token, $password) {
		// Check user exists
		$query = $this->CI->user_model->find(array('reset_token' => $token));
		
		if($query->num_rows() > 0) {
			$user = $query->row();
			
			// Update password
			$this->change_password($user->id, $password);
			
			return $user;			
		}
		
		return false;
	}
			
	public function change_password($user_id, $password) {
		// Update password
		$salt = $this->generate_random();
		$password = $this->hash_password($password, $salt);
		
		$this->CI->user_model->update($user_id, array(
			'salt' => $salt,
			'password' => $password,
			'reset_token' => ''
		));
	}
	
	public function hash_password($password, $salt) {
		return sha1($salt.$password);
	}
	
	private function generate_random() {
		$this->CI->load->helper('string');
		return random_string('alnum', 16);
	}
	
}